const schemas = require('../models/schema');
const _ = require('lodash');


const updatePrice = async () => {
    try {
        const stocks = await schemas.STOCK.find();
        _.each(stocks, stock => {
            stock.price = parseFloat((Math.random() * _.get(stock, 'price', 2)).toFixed(2)) + 1;
            stock.save();
        })
    } catch (err) {
        console.error("Error while updating price via cron. Error::", err);
    }
};

const getData = async () => {
    try {
        const stocks = await schemas.STOCK.find();
        let data = [];
        _.each(stocks, stock => {
            data.push({
                "code": stock.code,
                "price": stock.price
            })
        });
        return data;
    } catch (e) {
        console.error("Error while updating price via cron. Error::", err);
    }
};

module.exports = {
    updatePrice: updatePrice,
    getData: getData
};

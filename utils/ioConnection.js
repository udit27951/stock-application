const updateCron = require('./UpdateCron');


const initIO = function (io) {
    io.on('connection', socket => {
        console.info("IO connection establish");
        setInterval(async () => {
            try {
                let StockData = await updateCron.getData();
                socket.emit('price-change', {StockData});
            }catch (err) {
                console.error("initIO: Error while fetching updated data. Error::", err);
            }
        }, 5000)
    });
};

module.exports = {
    init : initIO
};

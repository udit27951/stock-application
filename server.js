const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const passport = require('passport');
const cron = require('node-cron');
const http = require('http');


const Strategy = require('./utils/validation');
const routers = require('./routes/index');
const updateCron = require('./utils/UpdateCron');
const initIO = require('./utils/ioConnection');


const app = express();
const httpServer = http.createServer(app);
const io = require('socket.io')(httpServer)

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.set('view engine', 'ejs');
app.set('views', __dirname + '/public/views');
app.set('layouts', 'layouts/layout');
app.use(express.static(`${__dirname}/public`));

app.use(session({
    secret: 'work hard',
    resave: true,
    saveUninitialized: false,
}));

app.use(passport.initialize());
app.use(passport.session());

app.use('/', routers);

passport.use(Strategy);

// initializing io
initIO.init(io);

mongoose.connect('mongodb+srv://udit:sockFinance@cluster0.c9fsy.mongodb.net/user?retryWrites=true&w=majority', {useNewUrlParser: true});
mongoose.connection.once('open', () => {
    console.log('connected to db');
    cron.schedule('*/10 * * * * *', async () => {
        await updateCron.updatePrice();
    });
}).on('error', (err) => {
    console.log("Error in connecting DB. Error::", err)
});




httpServer.listen('5000', ()=>{
   console.log('listening on port 5000')
});


const _ = require('lodash');

const schemas = require('../models/schema');


const setData = (schema, data) => {
    _.each(data, (value, index) => {
        if (index === 'stocks') {
            value = JSON.parse(value);
        }
        if (value) {
            schema[index] = value
        }
    });
    return schema;
};

const addData = (req, res) => {
    const data = req.body;
    const type = _.get(data, 'type', '').toUpperCase();
    let schema = new schemas[type]();
    schema = setData(schema, data, type);
    schema.save((err, saveObj) => {
        if (err) {
            console.error("addData:: Error in saving data. Error::", err);
            res.send('Error in saving data. Error::', err);
        } else {
            console.debug('data saved', saveObj);
            res.send('data saved');
        }
    })
};


const getDashboard = async (req, res) => {
    let context = {};
    try {
        const userData = await schemas.USER.findOne({email: req.user}).lean();
        const StockData = await schemas.STOCK.find();
        let myStock = userData.stocks;
        var myStockobj = myStock.reduce((obj, stock) => (obj[stock.code] = stock, obj), {});
        _.each(StockData, stock => {
            if (myStockobj[stock.code]) {
                myStockobj[stock.code].currentPrice = stock.price;
                delete stock.id;
            }
        });
        myStockobj = Object.entries(myStockobj).map((e) => (e[1]));
        myStockobj.sort((a, b) => (a.quantity > b.quantity) ? -1 : ((b.quantity > a.quantity) ? 1 : 0));
        context.profile = {
            name: userData.name,
            stock: myStockobj
        };
        res.render('dashboard', {data: context});
    } catch (err) {
        console.error("Unable to fetch data. Error::", err);
        res.send("Unable to fetch data. Error::", err);
    }
};

const buySellStock = async (req, res) => {
    const data = req.body;
    const email = _.get(req, 'body.email', '');
    const type = _.get(req, 'body.type', '');
    const code = _.get(req, 'body.code', '');
    try {
        const userData = await schemas.USER.findOne({email: email});
        let stocks = userData.stocks || [];
        if (type === 'buy') {
            let context = {
                name: _.get(req, 'body.name', ''),
                code: code,
                quantity: _.get(req, 'body.quantity', ''),
                buyingPrice: parseFloat(_.get(req, 'body.buyingPrice', ''))
            };
            stocks.push(context);
        }
        if (type === 'sell') {
            _.remove(stocks, (stock) => {
                return stock.code === code;
            })
        }
        userData.stocks = stocks;
        userData.markModified('stocks');
        userData.save((err, saveObj) => {
            if (err) {
                res.send('error', err);
            } else {
                console.log('data saved', saveObj);
                res.send('data saved');
            }
        });
    } catch (err) {
        console.error("Error while updating data. Error::", err);
        res.send("Error while updating data. Error::", err);
    }
};


module.exports = {
    addData: addData,
    getDashboard: getDashboard,
    buySellStock: buySellStock
};
